import { createGamePage, addUser, createTextHighlighters } from './helpers/domHelper.mjs';
import { showModal } from './modal/modal.mjs';

export const username = sessionStorage.getItem('username');

if (!username) {
  window.location.replace('/login');
}

export const socket = io('', { query: { username } });
export const roomsPage = document.getElementById('rooms-page');
export const gamePage = document.getElementById('game-page');
let currentRoomName;

socket.on('USER_EXISTS', (username) => {
  sessionStorage.setItem('username', '');
  window.location.replace('/login');
  alert(`Username ${username} already exists`);
});

const showGamePage = () => {
  roomsPage.classList.add('display-none');
  gamePage.classList.add('game');
  gamePage.classList.remove('display-none');
};

socket.on('JOIN_CREATED_ROOM', ({ roomName, users }) => {
  createGamePage(roomName);
  showGamePage();
  addUser(users[0]);
});

socket.on('JOIN_ROOM_DONE', ({ roomName, users }) => {
  currentRoomName = roomName
  gamePage.innerText = '';
  createGamePage(roomName);
  showGamePage();
  users.forEach((user) => {
    addUser(user);
  });
});

const getUsersElements = () => {
  return gamePage.querySelectorAll('.game__user-name');
};

socket.on('UPDATE_USER_STATUS', (users) => {
  const usersElements = getUsersElements();
  usersElements.forEach((userElement) => {
    const username = userElement.textContent;

    if (!users.hasOwnProperty(username)) {
      return;
    }

    const { isReady } = users[username];

    if (isReady) {
      userElement.classList.add('game__user-name--ready');
    } else {
      userElement.classList.remove('game__user-name--ready');
    }
  });
});

const finishGame = (users) => {
  const usersNames = Object.keys(users);
  showModal({ title: 'Winner', list: usersNames });
};

const emitProgress = (currentProgress) => {
  socket.emit('SET_PROGRESS', { username, progress: currentProgress, roomName: currentRoomName });
};

const setProgress = (text, textIndex) => {
  const textLength = text.length;
  const currentProgress = (100 * textIndex) / textLength;
  emitProgress(currentProgress);
};

const onKeyDown = ({ text, textHighlighters, gamePresentation, users }) => {
  const { typedTextElem, currentSymbolElem, remainingTextElem } = textHighlighters;
  let textIndex = 0;
  let nextIndex = 1;

  gamePresentation.innerText = text;
  return function keyDownHandler(event) {
    if (event.key !== text[textIndex]) {
      return;
    }
    let currentSymbol = text.slice(nextIndex, nextIndex + 1).replace(/ /g, '\u00A0');
    const typedText = text.slice(0, nextIndex).replace(/ /g, '\u00A0');
    const remainingText = text.slice(nextIndex + 1).replace(/ /g, '\u00A0');

    if (nextIndex === text.length) {
      currentSymbol = '';
    }

    typedTextElem.innerText = typedText;
    currentSymbolElem.innerText = currentSymbol;
    remainingTextElem.innerText = remainingText;

    gamePresentation.innerText = '';
    gamePresentation.append(typedTextElem, currentSymbolElem, remainingTextElem);
    setProgress(text, nextIndex);

    textIndex++;
    nextIndex++;

    if (textIndex === text.length) {
      finishGame(users);
      document.removeEventListener('keydown', keyDownHandler);
    }
  };
};

const showTimer = (currentSecond, gamePresentation) => {
  gamePresentation.innerText = currentSecond;
};

socket.on('START_GAME', ({ text, users, roomName, secondsBeforeStart }) => {
  const gamePresentation = gamePage.querySelector('.game__presentation');
  showTimer(secondsBeforeStart, gamePresentation);

  const intervalId = setInterval(({text, users, roomName, gamePresentation}) => {
    if (secondsBeforeStart !== 0) {
      secondsBeforeStart--;
      showTimer(secondsBeforeStart, gamePresentation);

    } else {
      clearInterval(intervalId);

      const textHighlighters = createTextHighlighters({ gamePresentation, text });
      
      document.addEventListener('keydown', onKeyDown({ text, textHighlighters, gamePresentation, users }));
    }
  }, 1000, {text, users, roomName, gamePresentation});
});

socket.on('USER_PROGRESS_UPDATED', (users) => {
  const usersNames = Object.keys(users);
  usersNames.forEach((name) => {
    const { progress } = users[name];
    const userContainer = gamePage.querySelector(`#${name}`);
    const progressBar = userContainer.querySelector('.game__user-progress-show');
    progressBar.style.width = progress + '%';
  });
});
