import { socket, username } from './../game.mjs';

//todo при переходе на страницу игры, удалить слушатели событий
export const createElement = ({ tagName, className, attributes = {} }) => {
  const element = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));

  return element;
};

const onJoinRoom = (roomName) => () => {
  socket.emit('JOIN_ROOM', { roomName, username });
};

const onCreateRoom = () => {
  const roomName = prompt('Room name');
  if (!roomName) {
    return;
  }
  socket.emit('CREATE_ROOM', { roomName, username });
};

export const createRoomCard = (roomName, usersConnected = 1) => {
  const roomCard = createElement({ tagName: 'div', className: 'room', attributes: { id: roomName } });
  const roomUsersCount = createElement({ tagName: 'p', className: 'room__users' });
  const usersCount = createElement({ tagName: 'span', className: 'room__users-count' });
  const usersCountText = createElement({ tagName: 'span', className: 'room__users-text' });
  const roomTitle = createElement({ tagName: 'p', className: 'room__name' });
  const joinRoom = createElement({ tagName: 'button', className: 'room__join' });

  usersCountText.innerText = 'Users connected: ';
  usersCount.innerText = usersConnected;
  roomUsersCount.append(usersCountText, usersCount);

  roomTitle.innerText = roomName;
  joinRoom.textContent = 'Join';
  joinRoom.addEventListener('click', onJoinRoom(roomName));

  roomCard.append(roomUsersCount, roomTitle, joinRoom);
  return roomCard;
};

export const createRoomsPage = () => {
  const roomsPage = document.getElementById('rooms-page');
  const title = createElement({ tagName: 'p', className: 'rooms-page__title' });
  const createNewRoom = createElement({
    tagName: 'button',
    className: 'rooms-page__create-room',
    attributes: { id: 'create-room' },
  });
  const roomsContainer = createElement({
    tagName: 'div',
    className: 'room-container',
    attributes: { id: 'room-container' },
  });

  title.innerText = 'Rooms Page';
  createNewRoom.innerText = 'Create room';

  createNewRoom.addEventListener('click', onCreateRoom);

  roomsPage.append(title, createNewRoom, roomsContainer);
  return roomsPage;
};

const onGoBackToRooms = () => {
  socket.emit('LEAVE__ROOM');
};

const onReady = (buttonElement, roomName) => {
  let isReady;
  return () => {
    if (!isReady) {
      buttonElement.innerHTML = 'not ready';
      socket.emit('USER_READY', { username, roomName });
      isReady = true;
    } else {
      buttonElement.innerHTML = 'ready';
      socket.emit('USER_NOT_READY', { username, roomName });
      isReady = false;
    }
  };
};

const createDashboard = (roomName) => {
  const dashboard = createElement({ tagName: 'div', className: 'game__dashboard' });
  const title = createElement({ tagName: 'p', className: 'game__title' });
  const goBack = createElement({ tagName: 'button', className: 'game__go-back' });
  const users = createElement({
    tagName: 'div',
    className: 'game__users',
    attributes: { id: 'game__users-container' },
  });

  title.innerText = roomName;
  goBack.innerText = 'Back To Rooms';
  goBack.addEventListener('click', onGoBackToRooms);

  dashboard.append(title, goBack, users);
  return dashboard;
};

const createPresentation = (roomName) => {
  const presentation = createElement({ tagName: 'div', className: 'game__presentation' });
  const getReady = createElement({ tagName: 'button', className: 'game__get-ready' });
  getReady.innerText = 'ready';
  getReady.addEventListener('click', onReady(getReady, roomName));

  presentation.append(getReady);
  return presentation;
};

export const createGamePage = (roomName) => {
  const gamePageContainer = document.getElementById('game-page');
  const dashboardContainer = createDashboard(roomName);
  const presentationContainer = createPresentation(roomName);
  gamePageContainer.append(dashboardContainer, presentationContainer);
};

export const addUser = (username) => {
  const usersContainer = document.getElementById('game__users-container');
  const userContainer = createElement({
    tagName: 'div',
    className: 'game__user-container',
    attributes: { id: username },
  });
  const userName = createElement({ tagName: 'p', className: 'game__user-name' });
  const userProgress = createElement({ tagName: 'div', className: 'game__user-progress' });
  const showUserProgress = createElement({ tagName: 'div', className: 'game__user-progress-show' });
  userProgress.append(showUserProgress);

  userName.innerText = username;

  userContainer.append(userName, userProgress);
  usersContainer.append(userContainer);
};

export const cleanUserList = () => {
  const usersContainer = document.getElementById('game__users-container');
  usersContainer.innerText = '';
};

export const createTextHighlighters = () => {
  const typedTextElem = createElement({ tagName: 'span', className: 'game__text-typed' });
  const currentSymbolElem = createElement({ tagName: 'span', className: 'game__current-symbol' });
  const remainingTextElem = createElement({ tagName: 'span', className: 'game__remaining-text' });
  return { typedTextElem, currentSymbolElem, remainingTextElem };
};
