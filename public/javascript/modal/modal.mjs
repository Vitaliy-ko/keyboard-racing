import { createElement } from './../helpers/domHelper.mjs';

export function showModal({ title, list, onClose = () => {} }) {
  const root = getModalContainer();
  const bodyContainer = createElement({ tagName: 'div', className: 'modal-container' });
  const bodyElements = list.map((itemName) => {
    const itemElement = createElement({ tagName: 'div', className: 'modal-item' });
    itemElement.innerText = itemName;
    return itemElement;
  });

  bodyContainer.append(...bodyElements);
  const modal = createModal({ title, bodyContainer, onClose });

  root.append(modal);
}

function getModalContainer() {
  return document.getElementById('root');
}

function createModal({ title, bodyContainer, onClose }) {
  const layer = createElement({ tagName: 'div', className: 'modal-layer' });
  const modalContainer = createElement({ tagName: 'div', className: 'modal-root' });
  const header = createHeader(title, onClose);

  modalContainer.append(header, bodyContainer);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title, onClose) {
  const headerElement = createElement({ tagName: 'div', className: 'modal-header' });
  const titleElement = createElement({ tagName: 'span' });
  const closeButton = createElement({ tagName: 'div', className: 'close-btn' });

  titleElement.innerText = title;
  closeButton.innerText = '×';

  const close = () => {
    hideModal();
    onClose();
  };
  closeButton.addEventListener('click', close);
  headerElement.append(titleElement, closeButton);

  return headerElement;
}

function hideModal() {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
