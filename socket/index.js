import {SECONDS_TIMER_BEFORE_START_GAME} from './config';
import { texts } from './../data';

const usersConnected = new Map();
export const rooms = {};

const setUserData = ({ roomName, username }) => {
  if (!rooms[roomName]) {
    rooms[roomName] = {};
  }

  rooms[roomName][username] = { progress: 0, isReady: false };
};

const checkForUsersReady = (roomName) => {
  const usersNames = Object.keys(rooms[roomName]);
  return usersNames.every((userName) => rooms[roomName][userName].isReady);
};

const startGame = (io, roomName) => {
  const isUsersReady = checkForUsersReady(roomName);
  if (isUsersReady) {
    const randomTextNumber = Math.floor(Math.random() * texts.length);
    const text = texts[randomTextNumber];
    io.to(roomName).emit('START_GAME', { text, roomName, users: rooms[roomName], secondsBeforeStart: SECONDS_TIMER_BEFORE_START_GAME });
  }
};

const updateUserData = ({ io, roomName, username, isReady }) => {
  rooms[roomName][username] = {
    progress: 0,
    isReady,
  };

  io.to(roomName).emit('UPDATE_USER_STATUS', rooms[roomName]);
};

const updateUserProgress = (io, username, progress, roomName) => {
  rooms[roomName][username].progress = progress;
  io.to(roomName).emit('USER_PROGRESS_UPDATED', rooms[roomName]);
};

export default (io) => {
  io.on('connection', (socket) => {
    const username = socket.handshake.query.username;
    const isUserNameExists = usersConnected.has(username);

    if (isUserNameExists) {
      socket.emit('USER_EXISTS', username);
      return;
    }

    usersConnected.set(username, socket.id);

    socket.emit('UPDATE_ROOMS', Object.entries(rooms));

    socket.on('disconnect', () => {
      usersConnected.delete(username);
    });

    socket.on('CREATE_ROOM', ({ roomName, username }) => {
      const isRoomExists = rooms.hasOwnProperty(roomName);
      if (isRoomExists) {
        socket.emit('ROOM_EXISTS');
        return;
      }

      setUserData({ roomName, username });
      socket.join(roomName, () => {
        socket.emit('JOIN_CREATED_ROOM', { roomName, users: [username] });
      });
      io.emit('NEW_ROOM_CREATED', roomName);
    });

    socket.on('JOIN_ROOM', ({ roomName, username }) => {
      const users = rooms[roomName];
      if (users.hasOwnProperty(username)) {
        return;
      }

      const usersCount = Object.keys(users).length;
      if (usersCount >= config.MAXIMUM_USERS_FOR_ONE_ROOM) {
        return;
      }

      setUserData({ roomName, username });
      socket.broadcast.emit('UPDATE_ROOMS', Object.entries(rooms));

      socket.join(roomName, () => {
        const users = Object.keys(rooms[roomName]);
        io.to(roomName).emit('JOIN_ROOM_DONE', { roomName, users });
      });
    });

    socket.on('USER_READY', ({ roomName, username }) => {
      updateUserData({ io, roomName, username, isReady: true });
      startGame(io, roomName);
    });

    socket.on('USER_NOT_READY', ({ roomName, username }) => {
      updateUserData({ io, roomName, username, isReady: false });
    });

    socket.on('SET_PROGRESS', ({ username, progress, roomName }) => {
      updateUserProgress(io, username, progress, roomName);
    });
  });
};
